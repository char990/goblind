#include "static_files.h"

const char * goblin_path = "/root";
const char * goblin_file = "/root/goblin";
const char * goblin_md5_file = "/root/goblin.md5";
const char * goblin_file_old = "/root/goblin.old";
const char * goblin_md5_file_old = "/root/goblin.md5.old";

const char * goblin_temp_path = "/root/goblin_temp";
const char * goblin_temp_file = "/root/goblin_temp/goblin";
const char * goblin_temp_md5_file = "/root/goblin_temp/goblin.md5";



const char * fifo_file = "/root/goblin_fifo";
const char * goblind_log_file = "/root/goblind.log";